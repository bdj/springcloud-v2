package com.cloud.v2.auth.config;

import com.alibaba.nacos.common.utils.MD5Utils;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 密码对比
 * @Author FuCE
 * @Date 2020/11/17 1:18
 * @Version 1.0
 **/
public class CustomPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    /**
     *  密码对比
     * @param rawPassword 传进来的秘密
     * @param encodedPassword 数据库密码
     * @return boolean
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        System.out.println("密码对比："+MD5Utils.md5Hex(rawPassword.toString(),"UTF-8")+"---"+encodedPassword);
        return MD5Utils.md5Hex(rawPassword.toString(),"UTF-8").equals(encodedPassword.toString());
    }
}
