package com.cloud.v2.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.cloud.v2.api.openapi")
@SpringCloudApplication
public class V2AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(V2AuthApplication.class, args);
    }
}
