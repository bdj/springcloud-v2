package com.cloud.v2.auth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName
 * @Description 请描述类的业务用途
 * @Author fuce
 * @Date 2020/11/17 0:22
 * @Version 1.0
 **/
@RestController
public class AuthEndpoint {
    protected Logger logger = LoggerFactory.getLogger(AuthEndpoint.class);

    @RequestMapping(value = { "/auth/user" }, produces = "application/json")
    @ResponseBody
    public Map<String, Object> user(OAuth2Authentication user) {
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("user", user.getUserAuthentication().getPrincipal());
        userInfo.put("authorities", AuthorityUtils.authorityListToSet( user.getUserAuthentication().getAuthorities()));
        return userInfo;
    }

    @RequestMapping(value = "/my", method = RequestMethod.GET)
    @ResponseBody
    public UserDto myDetail() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails) principal).getUsername();

        //String userName =  myUserDetails.getName();
        return new UserDto(username, username, "/avatar/default.png", "");
    }
}
