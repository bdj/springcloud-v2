package com.cloud.v2.auth.controller;

/**
 * @ClassName
 * @Description 请描述类的业务用途
 * @Author fuce
 * @Date 2020/11/17 1:11
 * @Version 1.0
 **/
public class UserDto {
    private String userName;
    private String userName1;
    private String s;
    private String s1;

    public UserDto(String userName, String userName1, String s, String s1) {
        this.userName = userName;
        this.userName1 = userName1;
        this.s = s;
        this.s1 = s1;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName1() {
        return userName1;
    }

    public void setUserName1(String userName1) {
        this.userName1 = userName1;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "userName='" + userName + '\'' +
                ", userName1='" + userName1 + '\'' +
                ", s='" + s + '\'' +
                ", s1='" + s1 + '\'' +
                '}';
    }

    public UserDto() {
    }
}
