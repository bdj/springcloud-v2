package com.cloud.v2.admin.mapper.custom;


import com.cloud.v2.admin.model.auto.SysPermission;

import java.util.List;

/**
 * 自定义权限Dao
 * @Author fuce
 * @Date 2020/11/21 0:03
 * @Version 1.0
 **/
public interface PermissionDao {
    /**
     * 查询全部权限
     * @return
     */
    List<SysPermission> findAll();

    /**
     * 根据用户id查询出用户的所有权限
     * @param userId
     * @return
     */
    List<SysPermission> findByAdminUserId(String userId);

    /**
     * 根据角色id查询权限
     * @param roleid
     * @return
     */
    List<SysPermission> queryRoleId(String roleid);

}
