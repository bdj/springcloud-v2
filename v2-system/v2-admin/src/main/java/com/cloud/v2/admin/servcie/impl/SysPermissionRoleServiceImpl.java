package com.cloud.v2.admin.servcie.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.v2.admin.mapper.auto.SysPermissionRoleMapper;
import com.cloud.v2.admin.model.auto.SysPermissionRole;
import com.cloud.v2.admin.servcie.ISysPermissionRoleService;
import org.springframework.stereotype.Service;

/**
 * 权限角色Service实现类
 * @Author: fuce
 * @Date: 2020/11/20
 **/
@Service
public class SysPermissionRoleServiceImpl  extends ServiceImpl<SysPermissionRoleMapper, SysPermissionRole> implements ISysPermissionRoleService{


}
