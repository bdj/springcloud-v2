package com.cloud.v2.admin.mapper.auto;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.v2.admin.model.auto.SysRole;


public interface SysRoleMapper extends BaseMapper<SysRole> {
}