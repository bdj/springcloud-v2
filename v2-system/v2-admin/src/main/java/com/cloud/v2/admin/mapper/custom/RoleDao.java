package com.cloud.v2.admin.mapper.custom;

import com.cloud.v2.admin.model.auto.SysRole;

import java.util.List;

/**
 * 自定义角色Dao
 * @Author: fuce
 * @Date: 2020/11/21
 **/
public interface RoleDao {
	/**
	 * 根据用户id查询角色
	 * @param userid
	 * @return
	 */
	List<SysRole> queryUserRole(String userid);
}
