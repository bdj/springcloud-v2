package com.cloud.v2.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@SpringCloudApplication
public class V2AdminApplication{

    public static void main(String[] args) {
        SpringApplication.run(V2AdminApplication.class, args);
    }
}
