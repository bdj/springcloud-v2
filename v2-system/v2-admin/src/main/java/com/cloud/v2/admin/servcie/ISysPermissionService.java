package com.cloud.v2.admin.servcie;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.v2.admin.model.auto.SysPermission;

import java.util.List;

/**
 * 权限service抽象类
 * @Author fuce
 * @Date 2020/11/20 23:10
 * @Version 1.0
 **/
public interface ISysPermissionService extends IService<SysPermission> {

    /**
     * 查询全部权限
     * @return
     */
    List<SysPermission> findAll();

    /**
     * 根据用户id查询出用户的所有权限
     * @param userId
     * @return
     */
    List<SysPermission> findByAdminUserId(String userId);

    /**
     * 根据角色id查询权限
     * @param roleid
     * @return
     */
    List<SysPermission> queryRoleId(String roleid);
}
