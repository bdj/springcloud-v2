package com.cloud.v2.admin.servcie;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.v2.admin.model.auto.SysRoleUser;

/**
 * 角色用户抽象类
 * @Author fuce
 * @Date 2020/11/18 0:52
 * @Version 1.0
 **/
public interface ISysRoleUserService extends IService<SysRoleUser> {


}
