package com.cloud.v2.admin.servcie.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.v2.admin.mapper.auto.SysPermissionMapper;
import com.cloud.v2.admin.mapper.custom.PermissionDao;
import com.cloud.v2.admin.model.auto.SysPermission;
import com.cloud.v2.admin.servcie.ISysPermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 权限service实现类
 * @Author fuce
 * @Date 2020/11/20 23:10
 * @Version 1.0
 **/
@Service
public class SysPermissionServiceImpl  extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    @Resource
    private PermissionDao permissionDao;

    /**
     * 查询全部权限
     * @return List<SysPermission>
     */
   public List<SysPermission> findAll(){
      return permissionDao.findAll();

   }

    /**
     * 根据用户id查询出用户的所有权限
     * @param userId
     * @return List<SysPermission>
     */
    public List<SysPermission> findByAdminUserId(String userId){
        return permissionDao.findByAdminUserId(userId);
    }

    /**
     * 根据角色id查询权限
     * @param roleId
     * @return List<SysPermission>
     */
    public List<SysPermission> queryRoleId(String roleId){
       return permissionDao.queryRoleId(roleId);
    }
}
