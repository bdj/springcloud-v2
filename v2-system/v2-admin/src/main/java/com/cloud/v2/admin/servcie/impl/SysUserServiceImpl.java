package com.cloud.v2.admin.servcie.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.v2.admin.mapper.auto.SysUserMapper;
import com.cloud.v2.admin.model.auto.SysUser;
import com.cloud.v2.admin.servcie.ISysUserService;
import org.springframework.stereotype.Service;

/**
 * 用户Service实现类
 * @Author fuce
 * @Date 2020/11/18 0:58
 * @Version 1.0
 **/
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper,SysUser> implements ISysUserService {


    @Override
    public SysUser queryName(String name) {
        LambdaQueryWrapper<SysUser> select = Wrappers.<SysUser>lambdaQuery().select().eq(SysUser::getUsername,name);
        return baseMapper.selectOne(select);
    }
}
