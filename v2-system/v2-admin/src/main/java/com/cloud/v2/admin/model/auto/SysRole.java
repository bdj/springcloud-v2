package com.cloud.v2.admin.model.auto;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * t_sys_role
 * @author 
 */
@TableName("t_sys_role")
public class SysRole implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 角色名称
     */
    private String name;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}