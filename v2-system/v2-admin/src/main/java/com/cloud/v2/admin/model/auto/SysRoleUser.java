package com.cloud.v2.admin.model.auto;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * t_sys_role_user
 * @author 
 */
@TableName("t_sys_role_user")
public class SysRoleUser implements Serializable {
    private String id;

    /**
     * 用户id
     */
    private String sysUserId;

    /**
     * 角色id
     */
    private String sysRoleId;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(String sysUserId) {
        this.sysUserId = sysUserId;
    }

    public String getSysRoleId() {
        return sysRoleId;
    }

    public void setSysRoleId(String sysRoleId) {
        this.sysRoleId = sysRoleId;
    }
}