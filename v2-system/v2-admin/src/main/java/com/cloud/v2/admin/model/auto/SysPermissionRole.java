package com.cloud.v2.admin.model.auto;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * t_sys_permission_role
 * @author 
 */
@TableName("t_sys_permission_role")
public class SysPermissionRole implements Serializable {
    private String id;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 权限id
     */
    private String permissionId;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }
}