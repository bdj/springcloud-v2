package com.cloud.v2.admin.mapper.auto;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.v2.admin.model.auto.SysPermission;

public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}