package com.cloud.v2.admin.servcie.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.v2.admin.mapper.auto.SysRoleUserMapper;
import com.cloud.v2.admin.model.auto.SysRoleUser;
import com.cloud.v2.admin.servcie.ISysRoleUserService;
import org.springframework.stereotype.Service;

/**
 * 角色用户Service实现类
 * @Author fuce
 * @Date 2020/11/18 0:58
 * @Version 1.0
 **/
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements ISysRoleUserService {


}
