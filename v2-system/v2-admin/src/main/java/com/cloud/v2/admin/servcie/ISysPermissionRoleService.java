package com.cloud.v2.admin.servcie;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.v2.admin.model.auto.SysPermissionRole;

/**
 * 权限角色抽象类
 * @Author fuce
 * @Date 2020/11/20 23:10
 * @Version 1.0
 **/
public interface ISysPermissionRoleService extends IService<SysPermissionRole> {
}
