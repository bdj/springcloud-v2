package com.cloud.v2.admin.servcie;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.v2.admin.model.auto.SysRole;

import java.util.List;

/**
 * 角色service抽象类
 * @Author fuce
 * @Date 2020/11/20 23:11
 * @Version 1.0
 **/
public interface ISysRoleService extends IService<SysRole> {
    /**
     * 根据用户id查询角色
     * @param userid
     * @return
     */
    List<SysRole> queryUserRole(String userid);
}
