package com.cloud.v2.admin.servcie;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.v2.admin.model.auto.SysUser;


/**
 * 用户抽象类
 * @Author fuce
 * @Date 2020/11/18 0:52
 * @Version 1.0
 **/
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据用户名字查询用户
     * @param name
     * @return
     */
    SysUser queryName(String name);

}
