package com.cloud.v2.admin.servcie.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.v2.admin.mapper.auto.SysRoleMapper;
import com.cloud.v2.admin.mapper.custom.RoleDao;
import com.cloud.v2.admin.model.auto.SysRole;
import com.cloud.v2.admin.servcie.ISysRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 角色Service实现类
 * @Author fuce
 * @Date 2020/11/20 23:11
 * @Version 1.0
 **/
@Service
public class SysRoleServiceImpl  extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private RoleDao roleDao;
    /**
     * 根据用户id查询角色
     * @param userid
     * @return List<SysRole>
     */
   public List<SysRole> queryUserRole(String userid){
     return roleDao.queryUserRole(userid);
   }
}
