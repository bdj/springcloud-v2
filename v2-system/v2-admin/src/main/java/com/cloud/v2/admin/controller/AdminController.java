package com.cloud.v2.admin.controller;

import com.cloud.v2.common.core.domain.AjaxResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName
 * @Description 请描述类的业务用途
 * @Author fuce
 * @Date 2020/11/15 20:45
 * @Version 1.0
 **/
@RestController
public class AdminController {

    @GetMapping("/index")
    public AjaxResult index(){
       return AjaxResult.success("哈哈");
    }

}
