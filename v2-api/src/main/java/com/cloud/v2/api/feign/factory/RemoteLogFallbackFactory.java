package com.cloud.v2.api.feign.factory;


import com.cloud.v2.api.feign.fallback.RemoteLogFallbackImpl;
import com.cloud.v2.api.openapi.RemoteUserService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 远程调用日志工厂
 * @Author: fuce
 * @Date: 2020/11/19
 **/
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteUserService> {

    @Override
    public RemoteUserService create(Throwable throwable) {
        return new RemoteLogFallbackImpl(throwable);
    }
}
