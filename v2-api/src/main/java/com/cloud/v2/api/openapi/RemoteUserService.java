package com.cloud.v2.api.openapi;

import com.cloud.v2.api.feign.factory.RemoteLogFallbackFactory;
import com.cloud.v2.api.model.LoginUser;
import com.cloud.v2.common.constant.ServiceNameConstants;
import com.cloud.v2.common.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 远程用户调用类
 * @Author fuce
 * @Date 2020/11/19 1:36
 * @Version 1.0
 **/
@FeignClient(contextId = "RemoteUserService", value = ServiceNameConstants.SYSTEM_ADMIN, fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteUserService {

    @RequestMapping(value = "/UserController/info/{username}", method = RequestMethod.GET)
    R<LoginUser> getInfo(@RequestParam("username") String username);
}
