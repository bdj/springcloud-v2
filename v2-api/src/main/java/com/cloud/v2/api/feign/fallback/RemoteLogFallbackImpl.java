package com.cloud.v2.api.feign.fallback;


import com.cloud.v2.api.model.LoginUser;
import com.cloud.v2.api.openapi.RemoteUserService;
import com.cloud.v2.common.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @Author: xuyiwei
 * @Email 1328312923@qq.com
 * @Description: 远程日志调用实现类
 * @Date: Create in 上午9:52 2019/10/21
 */
@Service
public class RemoteLogFallbackImpl implements RemoteUserService {
    private final static Logger logger = LoggerFactory.getLogger(RemoteLogFallbackImpl.class);

    private final Throwable throwable;

    public RemoteLogFallbackImpl(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public R<LoginUser> getInfo(String username) {
        logger.error("feign 调用用户{}出错,信息:{}",username,throwable.getLocalizedMessage());
        return null;
    }



}
