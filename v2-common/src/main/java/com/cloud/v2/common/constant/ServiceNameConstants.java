package com.cloud.v2.common.constant;

/**
 * 服务名称
 * 
 * @author ruoyi
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "v2-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "v2-system";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_ADMIN = "v2-admin";
}
