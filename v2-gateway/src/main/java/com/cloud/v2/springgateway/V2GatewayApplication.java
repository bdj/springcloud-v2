package com.cloud.v2.springgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


//网关自带feign接口  然后还要扫描feign下面的自动注入

@EnableFeignClients({"com.cloud.v2.springgateway","com.cloud.v2.auth"})
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class V2GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(V2GatewayApplication.class, args);
	}

}
