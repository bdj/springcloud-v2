package com.alibaba.nacos.console.config;

/**
 *覆盖nacos 默认配置
 * @Author: fuce
 * @Date: 2020/11/13
 **/
public interface ConfigNacos {

	/**
	 * nacos单机模式
	 */
	String STANDALONE_MODE = "nacos.standalone";

	/**
	 * 是否开启认证
	 */
	String AUTH_ENABLED = "nacos.core.auth.enabled";

}
